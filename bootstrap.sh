#!/usr/bin/env bash

cp hosts /etc/hosts
cp resolv.conf /etc/resolv.conf
yum install ntp -y
service ntpd start
service iptables stop
mkdir -p /root/.ssh; chmod 700 /root/.ssh; cp /home/vagrant/.ssh/authorized_keys /root/.ssh/

#Again, stopping iptables
/etc/init.d/iptables stop

# Increasing swap space
sudo dd if=/dev/zero of=/swapfile bs=1024 count=1024k
sudo mkswap /swapfile
sudo swapon /swapfile
echo "/swapfile       none    swap    sw      0       0" >> /etc/fstab


# Optionally add what you want:
echo '192.168.68.101 c6801' >> /etc/hosts
echo '192.168.68.102 c6802' >> /etc/hosts
echo '192.168.68.103 c6803' >> /etc/hosts
echo '192.168.68.104 c6804' >> /etc/hosts


sudo cp /home/vagrant/insecure_private_key /root/ec2-keypair
sudo chmod 600 /root/ec2-keypair
